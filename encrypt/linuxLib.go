//go:build linux

package encrypt

import "fmt"

type LinuxEncryptor struct {
}

func (e *LinuxEncryptor) PrintMessage() {
	fmt.Println("LINUX")

}

// fungsi ini akan dicall otomatis ketika package di load pertama kali
func init() {
	Encrypt = &LinuxEncryptor{}
}
