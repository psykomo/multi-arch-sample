package encrypt

import (
	"log"
)

type DefaultEncryptor struct {
}

func (e *DefaultEncryptor) PrintMessage() {
	log.Fatal("Architecture not supported. Supported on Linux and Windows only.")

}

// fungsi ini akan dicall otomatis ketika package di load pertama kali
func init() {
	Encrypt = &DefaultEncryptor{}
}
