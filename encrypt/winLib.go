//go:build windows

package encrypt

import "fmt"

type WinEncryptor struct {
}

func (e *WinEncryptor) PrintMessage() {
	fmt.Println("Windows")

}

// fungsi ini akan dicall otomatis ketika package di load pertama kali
func init() {
	Encrypt = &WinEncryptor{}
}
